# Generated by Django 3.1.14 on 2022-09-26 13:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('onboardingproject', '0024_auto_20220926_1316'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='loaninstallment',
            name='id',
        ),
        migrations.AlterField(
            model_name='loaninstallment',
            name='account_number_associated',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='onboardingproject.loan'),
        ),
    ]
