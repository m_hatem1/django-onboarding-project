# Generated by Django 3.1.14 on 2022-09-24 09:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('onboardingproject', '0010_auto_20220924_0937'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loan',
            name='account_number_associated',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='onboardingproject.bankaccount'),
        ),
    ]
