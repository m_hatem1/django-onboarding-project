from datetime import datetime
from urllib import request, response
from xmlrpc.client import ResponseError
from django.contrib.auth.models import User
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework import generics
from rest_framework.viewsets import GenericViewSet, mixins
from rest_framework.permissions import IsAdminUser
from .models import User

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, viewsets
from rest_framework import permissions

from .models import User, BankAccount, Loan, LoanInstallment
from .serializers import UserSerializer, BankAccountSerializer, LoanSerializer, LoanInstallmentSerializer
from rest_framework.generics import GenericAPIView

from dateutil.relativedelta import relativedelta

from rest_framework_swagger.views import get_swagger_view

from django.shortcuts import get_object_or_404

schema_view = get_swagger_view(title='Octo Onboarding Project API')


class UserListAPIView(viewsets.GenericViewSet, mixins.ListModelMixin,
                      mixins.RetrieveModelMixin, mixins.CreateModelMixin):

    queryset = User.objects.all()
    serializer_class = UserSerializer

    def update(self, request, *args, **kwargs):
        user_object = User.objects.get_object_or_404()
        data = request.data
        user_object.user_name = data["user_name"]
        user_object.save()
        serializer = UserSerializer(user_object)
        return Response(serializer.data)


class BankListAPIView(viewsets.GenericViewSet, mixins.ListModelMixin,
                      mixins.RetrieveModelMixin, mixins.CreateModelMixin, mixins.UpdateModelMixin):

    queryset = BankAccount.objects.all()
    serializer_class = BankAccountSerializer


class LoanListAPIView(viewsets.GenericViewSet, mixins.ListModelMixin,
                      mixins.RetrieveModelMixin, mixins.CreateModelMixin):
    queryset = Loan.objects.all()
    serializer_class = LoanSerializer

    # Type the url /loan/AccountNumber to get a list of all the loans of that user
    def retrieve(self, request, *args, **kwargs):
        account_number = self.kwargs['pk']

        instance = Loan.objects.filter(
            account_number_associated=account_number)
        serializer = LoanSerializer(instance, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):  # Take a loan

        loan_object = Loan()

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        account_number = serializer.data["account_number_associated"]
        bank_account = BankAccount.objects.get(
            account_number=account_number)
        loan_object.account_number_associated = bank_account
        loan_object.loan_amount = serializer.data["loan_amount"]

        # Loan installments should be created here
        loan_object.isOnLoan = False if serializer.data["loan_amount"] == 0 else True
        loan_object.save()

        if loan_object.isOnLoan:
            month_number = 1
            to_pay = float(loan_object.loan_amount) / 3
            while month_number <= 3:
                loan_installments_object = LoanInstallment()
                deadline_installment = loan_object.created + \
                    relativedelta(months=month_number)
                loan_installments_object.account_number_associated = loan_object
                loan_installments_object.payment_amount = to_pay
                loan_installments_object.month_installment = deadline_installment

                loan_installments_object.save()
                serializer = LoanInstallment(loan_installments_object)
                month_number += 1

        serializer = LoanSerializer(loan_object)
        return Response(serializer.data)


class LoanInstallmentAPIView(viewsets.GenericViewSet, mixins.ListModelMixin,
                             mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.CreateModelMixin):
    queryset = LoanInstallment.objects.all()
    serializer_class = LoanInstallmentSerializer

    def update(self, request, *args, **kwargs):
        accountID = self.kwargs['pk']

        BankAccount_object = BankAccount.objects.get(
            account_number=accountID)  # getting the bank account object with user bank account number

        LoanObject = Loan.objects.get(account_number_associated=accountID)

        installments = LoanInstallment.objects.filter(
            account_number_associated=LoanObject,
            is_paid=False)

        if len(installments) > 0:
            installments.order_by('month_installment')[0]
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if BankAccount_object.account_balance >= installments.payment_amount and not installments.is_paid:
            BankAccount_object.account_balance -= installments.payment_amount
            installments.is_paid = True
            BankAccount_object.save()
            installments.save()
        else:
            return ResponseError("Insufficient Funds", status.HTTP_304_NOT_MODIFIED)

        serializer = LoanInstallmentSerializer(installments)
        return Response(serializer.data)
