import json
from urllib import response

from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase

from onboardingproject.models import BankAccount, Loan, LoanInstallment, User
from onboardingproject.serializers import (BankAccountSerializer,
                                           LoanInstallmentSerializer,
                                           LoanSerializer, UserSerializer)


class RegisterUserTestCase(APITestCase):
    def setUp(self) -> None:
        # self.__set_api_urls__()
        # self.__set_request_samples__()
        # self.__set_up_objects__()

        self.createAUserUrl = '/users/'

    def test_registeration(self):
        print("test_registeration")
        data = {
            "user_name": "mohamed_test_case1",
            "password": "12345"
        }

        response = self.client.post(self.createAUserUrl, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        dataSecond = {
            "user_name": "mohamed_test_case1",
            "password": "12345"
        }

        response = self.client.post(self.createAUserUrl, dataSecond)
        self.assertTrue(response.status_code, status.HTTP_400_BAD_REQUEST)
        # response = self.client.post(url.format(id= ), data)
        # response.data
        # response.status_code
        print("=============================")

    def tearDown(self) -> None:
        User.objects.all().delete()
        BankAccount.objects.all().delete()
        Loan.objects.all().delete()
        LoanInstallment.objects.all().delete()


class BankAccountView(APITestCase):
    def setUp(self) -> None:
        self.bankAccountUrl = '/bankaccounts/'
        self.user = User(user_name="moh1", password="12345")
        self.user.save()

    def test_createBankAccount(self):
        # print("Here:", self.user.user_id)
        data = {
            "user_associated": 1,
            "account_balance": 250000000
        }
        response = self.client.post(self.bankAccountUrl, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def tearDown(self) -> None:
        User.objects.all().delete()
        BankAccount.objects.all().delete()
        Loan.objects.all().delete()
        LoanInstallment.objects.all().delete()
