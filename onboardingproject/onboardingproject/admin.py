from django.contrib import admin
from . import models


class OnboardingOcto(admin.AdminSite):
    site_header = 'Octo Onboarding Project'


onboarding_site = OnboardingOcto(name='OnboardingOctoAdmin')

onboarding_site.register(models.User)
onboarding_site.register(models.BankAccount)
onboarding_site.register(models.Loan)
onboarding_site.register(models.LoanInstallment)
