from email.policy import default
from django.db import models
from model_utils.models import TimeStampedModel
import uuid


class User(models.Model):
    user_id = models.AutoField(primary_key=True)
    date_created = models.DateTimeField(auto_now_add=True, null=False)
    user_name = models.CharField(max_length=100, unique=True)
    password = models.CharField(max_length=100, null=False)

    def __str__(self):  # to show on admin page the return of this function
        return str(self.user_id)


class BankAccount(TimeStampedModel):
    user_associated = models.ForeignKey(User, on_delete=models.CASCADE)
    account_number = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    account_balance = models.FloatField()

    def __str__(self):
        return str(self.account_number)


class Loan(TimeStampedModel):
    account_number_associated = models.ForeignKey(
        BankAccount, on_delete=models.CASCADE)
    loan_amount = models.FloatField()
    isOnLoan = models.BooleanField(default=True)  # 0 not on loan, 1 on loan

    def __str__(self):
        return str(self.account_number_associated)


class LoanInstallment(TimeStampedModel):
    account_number_associated = models.ForeignKey(
        Loan, on_delete=models.CASCADE)
    payment_amount = models.FloatField()
    is_paid = models.BooleanField(default=False, null=False)
    month_installment = models.DateTimeField()

    def __str__(self):
        return str(self.account_number_associated)
