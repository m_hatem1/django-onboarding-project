from rest_framework import serializers
from .models import User, BankAccount, Loan, LoanInstallment


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['user_id', 'date_created', 'user_name', 'password']


class BankAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankAccount
        fields = '__all__'


class LoanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Loan
        fields = ['account_number_associated',
                  'isOnLoan', 'loan_amount', 'created', 'modified']
        read_only_fields = ['isOnLoan']


class LoanInstallmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = LoanInstallment
        fields = ['account_number_associated',
                  'payment_amount', 'is_paid', 'month_installment']
