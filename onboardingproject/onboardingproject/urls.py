"""onboardingproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from email.mime import base
from .views import (
    UserListAPIView,
    BankListAPIView,
    LoanListAPIView,
    LoanInstallmentAPIView
)
from re import A
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from onboardingproject.admin import onboarding_site
from rest_framework.routers import DefaultRouter
from . import views
from django.urls import re_path
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        title="Onboarding Octo Project",
        default_version='v1',
        description="Test description",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,

)

user_list = views.UserListAPIView.as_view({
    'get': 'list'

})

bankAccounts_list = views.BankListAPIView.as_view({
    'get': 'list'
})

Loan_list = views.LoanListAPIView.as_view({
    'get': 'list'
})

Loan_Installments_list = views.LoanInstallmentAPIView.as_view({
    'get': 'list'
})


router = DefaultRouter()

router.register('users', views.UserListAPIView, basename="users")
router.register('bankaccounts', views.BankListAPIView, basename="bankaccounts")
router.register('loan', views.LoanListAPIView, basename="loan")
router.register('loaninstallment', views.LoanInstallmentAPIView,
                basename="loaninstallment")

urlpatterns = [
    path('', include(router.urls)),
    path("admin/", onboarding_site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('swagger', schema_view.with_ui('swagger',
                                        cache_timeout=0), name='schema-swagger-ui'),
]
